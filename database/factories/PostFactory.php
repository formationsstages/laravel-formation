<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {

    $start_date = $faker->dateTimeBetween('now', 'now +2 years', $timezone = null)->format('Y-m-d');
    $end_date = $faker->dateTimeBetween($start_date, $start_date.' + 1 month', $timezone = null)->format('Y-m-d');

    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'published_at' => $faker->dateTime(),
        'price' => $faker->numberBetween($min = 850, $max = 6000),
        'number_student' => $faker->numberBetween($min = 3, $max = 25),
        'start_date' => $start_date,
        'end_date' => $end_date,
        'post_type' => $faker->randomElement($array = array ('formation', 'stage')),
        'status' => $faker->randomElement($array = array ('published', 'unpublished'))
    ];
});
