<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk('local')->delete(Storage::allFiles());

        //create category
        App\Category::create([
            'name' => 'javascript'
        ]);
        App\Category::create([
            'name' => 'angular'
        ]);
        App\Category::create([
            'name' => 'laravel'
        ]);
        App\Category::create([
            'name' => 'html'
        ]);
        App\Category::create([
            'name' => 'symfony'
        ]);

        factory(App\Post::class, 30)->create()->each(function($post){

            $categorie = App\Category::find(rand(1,5));
            $post->categorie()->associate($categorie);
            $post->save(); 
            
            $link = str_random(12) . '.jpg'; 

            $file = file_get_contents('http://lorempicsum.com/futurama/250/250/' . rand(1, 9)); // flux
            Storage::disk('local')->put($link, $file);

            $post->picture()->create([
                'title' => 'Default', // valeur par défaut
                'link' => $link
            ]);

        });
    }

}
