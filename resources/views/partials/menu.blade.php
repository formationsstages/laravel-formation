<nav class="navbar navbar-expand-lg navbar-light bg-light mt-4 mb-4">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item">
          <a class="navbar-brand" href="{{ route('home')}}">Accueil</a>
          </li>
        <li class="nav-item">
            <a class="navbar-brand" href="{{route('post_type', 'stage')}}">Stage</a>
        </li>
        <li class="nav-item">
            <a class="navbar-brand" href="{{route('post_type', 'formation')}}">Formation</a>
        </li>
        <li class="nav-item">
            <a class="navbar-brand" href="{{route('contact')}}">Contact</a>
        </li>
        @include('partials.form_search')
      </ul>
      
      @include('partials.admin_menu')
    </div>
  </nav>

