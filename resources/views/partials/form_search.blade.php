<form action="{{ action('FrontController@search') }}" method="POST" class="form-inline">
@csrf
<input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search" value="" name="word">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
</form>
