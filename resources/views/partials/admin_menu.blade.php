@if(Auth::check())
<ul class="navbar-nav justify-content-end">
<li class="nav-item"><a class="navbar-brand" href="{{route('post.index')}}">Dashboard</a></li>
<li class="nav-item"><a class="navbar-brand" href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Se déconnecter</a></li>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf 
    </form>
    @else
    
    @endif
</ul>