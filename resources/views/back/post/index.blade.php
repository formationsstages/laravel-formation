@extends('layouts.master')

@section('content')

@include('partials.flash')


<a href="{{route('post.create')}}" class="btn btn-info mb-4">
Ajouter un post</a>
<a href="{{route('trash')}}" class="btn btn-info mb-4">
  Corbeille</a>

{{$posts->links()}}
<form class="delete" method="post" action="{{ route('deleteAll') }}">
    @csrf
    <button type="submit" class="btn btn-primary mb-2 float-right">Mettre dans la corbeille</button> 
<table class="table table-striped">
    <thead class="table-primary">
        <tr>
            <th>Titre</th>
            <th>Prix</th>
            <th>Début</th>
            <th>Fin</th>
            <th>Nombre d'élèves</th>
            <th>Type</th>
            <th>Date de publication</th>
            <th>Date de création</th>
            <th>Date de mise à jour</th>
            <th>Status</th>
            <th>Modifier</th>
            <th>Voir</th>
            <th>Supprimer</th>
        </tr> {{-- End of tr, container of the dashboard's first line --}}
    </thead>
    <tbody>
    @forelse($posts as $post)
    <tr>
      <td>
        <a href="{{route('post.edit', $post->id)}}">{{$post->title}}</a>
      </td>

      <td>{{ $post->price}}</td>

      <td>{{ $post->start_date}}</td>

      <td>{{ $post->end_date}}</td>

      <td>{{ $post->number_student}}</td>

      <td>{{ $post->post_type}}</td>

      <td>{{$post->published_at}}</td>

      <td>{{$post->created_at}}</td>

      <td>{{$post->updated_at}}</td>

      <td>
      @if($post->status == 'published')
        <button type="button" class="btn btn-success">Publié</button>
      @else
      <button type="button" class="btn btn-warning">Non publié</button>
      @endif
      </td>

      <td>
        <a href="{{route('post.edit', $post->id)}}"><i class="far fa-edit"></i></a>
      </td>
      
      <td>
        <a href="{{route('post.show', $post->id)}}"><i class="fas fa-eye"></i></a></td>
      <td>
        <input type="checkbox" class="form-check-input" id="delete_all" name="delete[]" value="{{ $post->id }}">
      </td>
    </tr>
    @empty
        Aucun titre
    @endforelse
  </tbody>{{-- End of tbody, container of the table --}}
</table>{{-- /.table .table-striped --}}

<button type="submit" class="btn btn-primary mb-2 float-right">Mettre dans la corbeille</button> 
</form>

{{$posts->links()}}
@endsection

@section('scripts')
    @parent
        <script src="{{asset('js/confirm.js')}}"></script>
@endsection