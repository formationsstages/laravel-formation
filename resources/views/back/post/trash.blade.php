@extends('layouts.master')

@section('content')
@include('partials.flash')

{{$trashedPosts->links()}}
<form class="destroy" method="post" action="{{ route('destroy') }}">
      @csrf
      <button type="submit" class="btn btn-primary mb-2 float-right">Supprimer définitivement</button> 
<table class="table table-striped">
    <thead class="table-primary">
        <tr>
            <th>Titre</th>
            <th>Prix</th>
            <th>Début</th>
            <th>Fin</th>
            <th>Nombre d'élèves</th>
            <th>Type</th>
            <th>Date de publication</th>
            <th>Date de création</th>
            <th>Date de mise à jour</th>
            <th>Status</th>
            <th>Voir</th>
            <th>Supprimer</th>
            <th>Restaurer</th>
        </tr> {{-- End of tr, container of the dashboard's first line --}}
    </thead>
    <tbody>
    @forelse($trashedPosts as $key=>$trashedPost)
    <tr>
      <td>{{$trashedPost->title}}</td>

      <td>{{ $trashedPost->price}}</td>

      <td>{{ $trashedPost->start_date}}</td>

      <td>{{ $trashedPost->end_date}}</td>

      <td>{{ $trashedPost->number_student}}</td>

      <td>{{ $trashedPost->post_type}}</td>

      <td>{{$trashedPost->published_at}}</td>

      <td>{{$trashedPost->created_at}}</td>

      <td>{{$trashedPost->updated_at}}</td>

      <td>
        @if($trashedPost->status == 'published')
          <button type="button" class="btn btn-success">Publié</button>
        @else
          <button type="button" class="btn btn-warning">Non publié</button>
        @endif
      </td>

      <td>
        <a href="{{route('post.show', $trashedPost->id)}}"><i class="fas fa-eye"></i></a>
      </td>

      <td>
        <input type="checkbox" class="form-check-input" id="destroy" name="destroy[]" value="{{ $trashedPost->id }}">
      </td>

      <td>
          <a href="{{ action('PostController@restore', ['id' => $trashedPost->id]) }}" class="btn btn-danger">Restaurer</a>
      </td>
    </tr>
    @empty
        Aucun titre
    @endforelse
  </tbody>{{-- End of tbody, container of the table --}}
</table>{{-- /.table .table-striped --}}

<button type="submit" class="btn btn-primary mb-2 float-right">Supprimer définitivement</button> 
</form>

{{$trashedPosts->links()}}
@endsection

@section('scripts')
    @parent
        <script src="{{asset('js/confirm.js')}}"></script>
@endsection