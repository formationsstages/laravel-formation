@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-6">

    <h1>Modifier le stage/ la formation</h1>

    <form action="{{route('post.update', $post->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    {{method_field('PUT')}}

        <div class="form">
            <div class="form-group">
                <label for="title">Titre :</label>
                <input type="text" name="title" value="{{$post->title}}" class="form-control" id="title" placeholder="Titre de la formation ou du stage">
                @if($errors->has('title')) <span class="error bg-warning text-warning">{{$errors->first('title')}}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="description">Description :</label>
                <textarea type="text" name="description" class="form-control" >{{$post->description}}</textarea>
                @if($errors->has('description')) <span class="error bg-warning text-warning">{{$errors->first('description')}}</span>
                @endif
            </div>
        </div>
                    
        <div class="form-select">
            <label for="categorie">Catégorie :</label>
            <select id="categorie" name="categorie_id">
            <option value="0" {{is_null($post->categorie)? 'selected' : ''}}>Aucune catégorie</option>
            @foreach($categories as $id => $name)
                <option {{ ($post->categorie_id == $id) ? 'selected' : '' }} value="{{$id}}">{{$name}}</option>
            @endforeach
             </select>
        </div>
                    
        <div class="form-check form-check-inline">
            <label for="post_type">Type :</label>
            <input class="form-check-input" type="radio" id="post_type"
            @if($post->post_type == 'formation')
            checked
            @endif
            name="post_type" value="formation">
            <label class="form-check-label" for="post_type">Formation</label>
            <input class="form-check-input" type="radio" id="post_type"
            @if($post->post_type == 'stage')
            checked
            @endif
            name="post_type" value="stage">
            <label class="form-check-label" for="post_type">Stage</label>
        </div>

        <div class="form-group">
            <label for="price">Prix (en euros) :</label>
            <textarea type="number" name="price" class="form-control">{{$post->price}}</textarea>
            @if($errors->has('price'))
            <span class="error bg-warning text-warning">{{$errors->first('price')}}</span>
            @endif
        </div>

        <div class="form-group">
            <label for="number_student">Nombre d'élève admis :</label>
            <textarea type="number" name="number_student" class="form-control">{{$post->number_student}}</textarea>
            @if($errors->has('number_student'))
            <span class="error bg-warning text-warning">{{$errors->first('number_student')}}</span>
            @endif
        </div>
                    
</div><!-- /. col-6 -->

<div class="row justify-content-center">
<div class="col-md-6">
    <button type="submit" class="btn btn-primary">Modifiez</button>
    
    <div class="input-radio">
        <label for="status">Status :</label>
        <input type="radio" id="status" @if(old('status')=='published') checked @endif name="status" value="published" checked>
        <label class="form-check-label" for="post_type">Publié</label>
        <input type="radio" id="status" @if(old('status')=='unpublished') checked @endif name="status" value="unpublished" >
        <label class="form-check-label" for="post_type">Non publié</label>
    </div>

    <div class="form-group">
            <label for="start_date">Début :</label>
            <input type="date" id="start_date" name="start_date" value={{ $post->start_date }}>
    </div>
                    
    <div class="form-group">
            <label for="end_date">Fin :</label>
            <input type="date" id="end_date" name="end_date" value={{ $post->end_date }}>
    </div>

    @if($post->picture)
    <div class="form-group">
        <p>Image associée :</p>
        <label for="title_image">Titre image :</label>
        <input type="text" name="title_image" value="{{ $post->picture->title }}">

        <input class="file" type="file" name="picture" id="title_image">

        @if($errors->has('picture'))
        <span class="error bg-warning text-warning">{{$errors->first('picture')}}</span>
        @endif
    </div>

    <div class="form-group">
        <img width="200" src="{{ url('images', $post->picture->link) }}" alt="">
    </div>
    @endif
</div><!-- /.col md 6 -->
</div><!-- /.row .justify-content-center -->
</form>
</div>{{-- /.row --}}
@endsection