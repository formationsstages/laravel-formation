@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-6">

    <h1>Créer un post</h1>

    <form action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
    @csrf

    <div class="form">
            <div class="form-group">
                <label for="title">Titre :</label>
                <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Titre de la formation ou du stage">
                @if($errors->has('title')) <span class="error bg-warning text-warning">{{$errors->first('title')}}</span>
                @endif
            </div>

            <div class="form-group">
                    <label for="description">Description :</label>
                    <textarea type="text" name="description" class="form-control" >{{ old('description') }}</textarea>
                    @if($errors->has('description')) <span class="error bg-warning text-warning">{{$errors->first('description')}}</span>
                    @endif
                </div>
            </div>

            <div class="form-select">
                    <label for="categorie">Catégorie :</label>
                    <select id="categorie" name="categorie_id">
                    <option value="0" {{is_null(old('categorie_id'))? 'selected' : ''}}>Aucune catégorie</option>
                    @forelse($categories as $id => $name)
                        @if(old('categorie_id') == $id)
                        <option value="{{ $id }}" selected>{{ $name }}</option>
                        @else
                        <option value="{{ $id }}">{{ $name }}</option>
                        @endif
                    @empty
                        <option>Aucune catégorie</option>
                    @endforelse
                     </select>
                </div>
            

                <div class="form-check form-check-inline">
                        <label for="post_type">Type:</label>
                        <input class="form-check-input" type="radio" id="post_type"
                        @if(old('post_type') == 'formation')
                        checked
                        @endif
                        name="post_type" value="formation">
                        <label class="form-check-label" for="post_type">Formation</label>
                        <input class="form-check-input" type="radio" id="post_type"
                        @if(old('post_type') == 'stage')
                        checked
                        @endif
                        name="post_type" value="stage">
                        <label class="form-check-label" for="post_type">Stage</label>
                    </div>

                    <div class="form-group">
                            <label for="price">Prix (en euros) :</label>
                            <textarea type="number" name="price" class="form-control">{{ old('price') }}</textarea>
                            @if($errors->has('price'))
                            <span class="error bg-warning text-warning">{{$errors->first('price')}}</span>
                            @endif
                        </div>
                    
                        <div class="form-group">
                                <label for="number_student">Nombre d'élève admis :</label>
                                <textarea type="number" name="number_student" class="form-control"> {{ old('number_student') }}</textarea>
                                @if($errors->has('number_student'))
                                <span class="error bg-warning text-warning">{{$errors->first('number_student')}}</span>
                                @endif
                            </div>

</div><!-- /. col-6 -->

<div class="row justify-content-center">
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary">Ajouter un post</button>
            
            @include('partials.errors')

            <div class="input-radio">
                    <label for="status">Status :</label>
                    <input type="radio" id="status" @if(old('status')=='published') checked @endif name="status" value="published" checked>
                    <label class="form-check-label" for="post_type">Publié</label>
                    <input type="radio" id="status" @if(old('status')=='unpublished') checked @endif name="status" value="unpublished" >
                    <label class="form-check-label" for="post_type">Non publié</label>
                </div>

                <div class="form-group">
                        <label for="start_date">Début :</label>
                        <input type="date" id="start_date" name="start_date" value={{ old('start_date') }}>
                </div>
                                
                <div class="form-group">
                        <label for="end_date">Fin :</label>
                        <input type="date" id="end_date" name="end_date" value={{ old('end_date') }}>
                </div>

                    
                    <div class="form-group">

                        <label for="title_image">Titre image :</label>

                        <input type="text" name="title_image" value="{{ old('title_image') }}">

                        <input class="file" type="file" name="picture" id="title_image">

                        @if($errors->has('picture'))
                        <span class="error bg-warning text-warning">{{$errors->first('picture')}}</span>
                        @endif
                    </div>
                    

        </div><!-- /.col md 6 -->
</div><!-- /.row .justify-content-center -->
</form>
</div>{{-- /.row --}}
@endsection


