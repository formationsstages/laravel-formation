@extends('layouts.master') 
@section('content')

<div class="col-9">
    <ul class="list-group">
        @forelse($posts as $post)
            <li class="list-group-item mb-3">
                <h3>
                    <a href="{{ action('FrontController@show', ['id' => $post->id]) }}">{{$post->title}}</a>
                </h3>
                
                <div class="row mt-3">
                    @if(is_null($post->picture) == false)
                        <div class="col-3">
                            <a href="#" class="thumbnail">
                                <img width="171" src="{{asset('images/'.$post->picture->link)}}" alt="{{$post->picture->title}}">
                            </a>
                        </div>{{-- /.col-3 --}}
                    @endif
                        <div class="col-8 ml-3">{{$post->description}}
                        </div>
                </div>{{-- /.row mt-3 --}}

                <div class="row mt-3">
                    <div class="col-3">Prix: {{$post->price}}€
                    </div>
                    <div class="col-3">Type: {{$post->post_type}}</div>
                    <div class="col-3">Début: {{$post->start_date}}</div>
                </div>{{-- /.row mt-3 --}}
            </li>{{-- /.list-group-item --}}
        @empty
            <li>Désolé nous n'avons pas de nouvelles formations ou de nouveau stage pour le moment</li>
        @endforelse
    </ul>{{-- /.list-group --}}
</div>{{-- /.col-8 --}}
@endsection