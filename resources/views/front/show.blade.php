@extends('layouts.master') 
@section('content')
<article>
    @if(is_null($post) == false)
        <h1>{{$post->title}}</h1>
            <div class="row mt-3">
                @if(is_null($post->picture) == false)
                    <div class="col">
                        <a href="#" class="thumbnail">
                            <img width="300" src="{{asset('images/'.$post->picture->link)}}" alt="{{$post->picture->title}}">
                        </a>
                    </div> {{-- /.col --}}
                @endif
                <div class="col-8">
                    <h4>Description :</h4>
                    <p>{{$post->description}}</p>
                </div>
            </div>{{-- row mt-3--}}

            <div class="row mt-4">

                <div class="col">
                    <h4>Début: </h4>
                    <p>{{$post->start_date}}</p>
                </div>

                <div class="col">
                    <h4>Prix: </h4>
                    <p>{{$post->price}}€</p>
                </div>

                <div class="col">
                    <h4>Fin: </h4>
                    <p>{{$post->end_date}}</p>
                </div>

                <div class="col">
                    <h4>Nombre d'élève: </h4>
                    <p>{{$post->number_student}}</p>
                </div>

                <div class="col">
                    <h4>Type: </h4>
                    <p>{{$post->post_type}}</p>
                </div>

                <div class="col">
                    <h4>Catégorie: </h4>
                    <p>{{$post->categorie->name}}</p>
                </div>

    @else
        <h3>Désolé aucune formation, aucun stage trouvé</h3>
    @endif
            </div> {{-- /.row --}}
</article> {{-- End of article --}}
@endsection