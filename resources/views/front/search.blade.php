@extends('layouts.master') 
@section('content')

<div class="col-9">
    <ul class="list-group">
        @forelse($results_search as $result_search)
            <li class="list-group-item mb-3">
                <h3>
                    <a href="{{ action('FrontController@show', ['id' => $result_search->id]) }}">{{$result_search->title}}</a>
                </h3>

            <div class="row mt-3">
                @if(is_null($result_search->picture) == false)
                    <div class="col-3">
                        <a href="#" class="thumbnail">
                        <img width="171" src="{{asset('images/'.$result_search->picture->link)}}" alt="{{$result_search->picture->title}}">
                        </a>
                    </div>{{-- /.col-3 --}}
                @endif
                    <div class="col-8 ml-3">{{$result_search->description}}
                    </div>
            </div>{{-- /.row mt-3 --}}


            <div class="row mt-3">
                <div class="col-3">Prix: {{$result_search->price}}€
                </div>
                <div class="col-3">Type: {{$result_search->post_type}}</div>
                <div class="col-3">Début: {{$result_search->start_date}}</div>
            </div>{{-- /.row --}}
            </li>{{-- /.list-group-item --}}
        @empty
            <li>Désolé aucun résultat</li>
        @endforelse
    </ul>{{-- /.list-group --}}
</div>{{-- /.col-9 --}}
@endsection