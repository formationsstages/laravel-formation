@extends('layouts.master') 
@section('content')
@if(Session::has('message'))
{{ Session::get('message') }}
@else
<h2>Contact</h2>
<form action="{{ action('FrontController@sendMail')}}" method="POST">
    @csrf
    <div class="form-group">
      <label for="email_user_request">Votre Email</label>
      <input name="email" type="email" class="form-control" id="email_user_request" aria-describedby="emailHelp" placeholder="Entrez votre email" value="" required>
    </div>
    <div class="form-group">
      <label for="user_request">Votre demande</label>
      <textarea name="content" type="text" class="form-control" value="" required></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endif
@endsection