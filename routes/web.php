<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'FrontController@index')->name('home');

// binding de model
Route::get('post/{post}', 'FrontController@show');

Route::get('posts/{post_type}', 'FrontController@showPostByType')->name('post_type');

Route::get('contact', 'FrontController@contact')->name('contact');

Route::post('search','FrontController@search');

Route::post('email', 'FrontController@sendMail');

Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::resource('admin/post', 'PostController');
    Route::post('delete', 'PostController@deleteAll')->name('deleteAll');
    Route::get('trash','PostController@trash')->name('trash');
    Route::post('destroy', 'PostController@destroy')->name('destroy');
    Route::get('restore/{id}', 'PostController@restore')->name('restore');
});


//Route::get('post/{type}/{name?}', 'FrontController@showPostByTypes')->name('type'); pour le référencement
