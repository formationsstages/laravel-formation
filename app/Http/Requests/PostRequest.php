<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required',
            'description' => 'required|string',
            'categorie_id' => 'integer',
            'title_image' => 'string|nullable',
            'picture' => 'mimes:jpeg,jpg|max:3000',
            'start_date' => 'date',
            'end_date' => 'date',
            //'price' => 'required|number',
            'number_student' => 'integer',
            'post_type' => 'in:formation,stage',
            'status' => 'in:published,unpublished',
        ];
    }
}
