<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Picture;
use Mail;

class FrontController extends Controller
{
    protected $paginate = 5;

    //SHOW THE LAST TWO POSTS ON THE INDEX PAGE
    public function index(){

        $posts = Post::orderBy('start_date', 'desc')->with('picture')->limit(2)->published()->get();

        return view('front.index', compact('posts'));
        
    }

    //SHOW ONE POST
    public function show(Post $post){

        return view('front.show', ['post' => $post]);

    }

    //SHOW POST BY TYPE
    public function showPostByType(string $post_type){
        
        $posts = Post::Type($post_type)->paginate($this->paginate)->published();

        
        return view('front.type', compact('posts', 'post_type') );
    }

    //SHOW CONTACT PAGE
    public function contact(){

        return view('front.contact');
    }

    //TEST FOR SENDING EMAILS
    public function sendMail(Request $request){

        $this->validate($request, [
            
            'email' => 'email',
            'content' => 'string'
        ]);

        Mail::send('emails.send', ['data' => $request], function ($m)   {
            $m->from('hello@app.com', 'Your Application');
            $m->to('tony@tony.fr', 'Tony')->subject('Your Reminder!');
        });
        
        return back()->with('message', 'Votre demande a bien été prise en compte');

    }

    //SHOW SEARCH'S RESULTS
    public function search(Request $request){

        $this->validate($request, [
            
            'word' => 'string'
        ]);

        $results_search = Post::search($request->word)->paginate($this->paginate);
        
        return view('front.search', ['results_search' => $results_search]);
    }

}


