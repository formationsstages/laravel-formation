<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Picture;
use App\Http\Requests\PostRequest;
use Storage;
use Cache;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * AFFICHER TOUS LES POSTS
     */

    protected $paginate = 10;

    public function index()
    {
        $posts = Post::paginate($this->paginate);

        return view('back.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * CREER UN POST
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');

       return view('back.post.create', compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     */

    public function store(PostRequest $request)
    {

        $post = Post::create($request->all());
        $im = $request->file('picture');

        if(!empty($im)){
            
            $link = $request->file('picture')->store('/.');
            $post->picture()->create([
            'link' => $link,
            'title' => $request->title_image ?? $request->title
            ]);
        }

        Cache::flush();

        return redirect()->route('post.index')->with('message', 'Votre post a bien été créé !');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * Montrer un livre
     */
    public function show(Post $post)
    {

        return view('back.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        
        $categories = Category::pluck('name', 'id')->all();
       return view('back.post.edit', compact ('post', 'categories'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {

        $post->update($request->all());

        $im = $request->file('picture');

        if(!empty($im)){
            
            $link = $request->file('picture')->store('./');

            if(!is_null($post->picture)){

                Storage::disk('local')->delete($post->picture->link);

                $post->picture()->delete();
            }
            $post->picture()->create([
                'link' => $link,
                'title' => $request->title_image?? $request->title

            ]);
        }

        Cache::flush();

        return redirect()->route('post.index')->with('message','Le post a été modifié !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $collection = collect($request->destroy);
        $collection->each(function($id, $key){
            $post = Post::onlyTrashed()->find($id)->forceDelete();
        });

        Cache::flush();

        return redirect()->route('trash')->with('message','Les posts ont été supprimés avec succès !');
    }

    //PUT POST IN TRASH
    public function deleteAll(Request $request){

        $collection = collect($request->delete);
        $collection->each(function($id, $key){

            $post = Post::find($id)->delete();

        });

        Cache::flush();

        return redirect()->route('post.index')->with('message','Les posts ont été placés dans la corbeille !');

    }

    //POST IN TRASH
    public function trash(){

        $trashedPosts = Post::onlyTrashed()->paginate($this->paginate);
        
        return view('back.post.trash',compact('trashedPosts'));
    }

    //RESTORE POST
    public function restore(int $id){
        $post = Post::onlyTrashed()
                    ->where('id', $id)
                    ->restore();

        return redirect()->route('post.index')->with('message','Les posts ont été restaurés correctement !');
    }

}
