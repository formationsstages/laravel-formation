<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{

    use SoftDeletes;

    protected $guarded = ['title_image', 'picture'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public function setCategorieIdAttribute($value){
       
        if($value == 0){
            $this->attributes['categorie_id'] = null;
        }else{

            $this->attributes['categorie_id'] = $value;
        }

    }

    public function scopeType($query, $type){
        return $query->where('post_type', $type);
    }

    public function scopeSearch($query, string $word){
        return $query->where('title', 'like', "%$word%");
    }

    public function scopePublished($query){
        return $query->where('status', 'published');
    }

    public function categorie(){
        return $this->belongsTo(Category::class);
    }

    public function picture(){
        return $this->hasOne(Picture::class);
    }
    
}
